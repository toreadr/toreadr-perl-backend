#!/bin/sh

APPNAME="toreadr"

echo "*"
echo "* Creating Toreadr app on OpenShift..."
echo "*"

rhc app create $APPNAME perl-5.10 cron-1.4

echo "*"
echo "* Preparing Toreadr sources..."
echo "*"

cd $APPNAME
git rm -r perl
git rm README.md

git commit -a -m "remove stock perl dir to prepare for toreader, remove stock README"
git remote add upstream -m master git@bitbucket.org:toreadr/toreadr-perl-backend.git
git pull -s recursive -X theirs upstream master

echo "*"
echo "* Deploying Toreadr to OpenShift..."
echo "*"

git push

